#include "NoteScheduler.h"
#include "VoiceAllocator.h"
#include <algorithm>
#include <iterator>
#include <functional>


NoteScheduler::NoteScheduler(VoiceAllocator *pVoiceAlloc)
   :  _pVoiceAlloc(pVoiceAlloc),
      _nFrames(0),
      _nNextNote(-1)
{ }


void NoteScheduler::enqueue(MIDINote::NoteType type, unsigned char note, unsigned char velocity, int deltaFrames)
{
   MIDINote newNote(type, note, velocity, deltaFrames);

   if (deltaFrames >= _nFrames) {
      _futureNotes.push_back(newNote);
   }
   else {
      _notes.push_back(newNote);
      sortNotes(_notes);
      _nNextNote = 0;
   }
} // NoteScheduler::enqueue


void NoteScheduler::setPosition(int nFrame)
{
   if ((_nNextNote == -1) || (_nNextNote >= (int) _notes.size())) return;

   while ((_nNextNote < (int) _notes.size()) && (_notes[_nNextNote].DeltaFrames <= nFrame)) {
      const MIDINote &note = _notes[_nNextNote];

      switch (note.Type) {
      case MIDINote::NoteType::On:
         _pVoiceAlloc->noteOn(note.Note);
         break;

      case MIDINote::NoteType::Off:
         _pVoiceAlloc->noteOff(note.Note);
         break;
      }

      ++_nNextNote;
   }
} // NoteScheduler::setPosition


void NoteScheduler::nextFragment(int nFrames)
{
   _nFrames = nFrames;
} // NoteScheduler::nextFragment


void NoteScheduler::fragmentDone()
{
   if (!_futureNotes.empty()) {
      _notes = _futureNotes;
      _futureNotes.clear();

      // Move notes which are still in the next frame into the list of future notes
      NoteList::const_iterator iNote, iEnd;
      for (iNote = _notes.cbegin(), iEnd = _notes.end(); iNote < iEnd;) {
         if (iNote->DeltaFrames >= _nFrames) {
            iNote = _notes.erase(iNote);
            _futureNotes.push_back(*iNote);
         }
         else ++iNote;
      }

      if (_notes.empty()) _nNextNote = -1;
      else {
         sortNotes(_notes);
         _nNextNote = 0;
      }
   }
   else {
      _notes.clear();
      _nNextNote = -1;
   }
} // NoteScheduler::fragmentDone