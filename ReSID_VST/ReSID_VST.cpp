#include "ReSID_VST.h"
#include "../resid-0.16/sid.h"
#include "MonophonicVoiceAlloc.h"
#include "NoteScheduler.h"
#include "MIDINote.h"
#include "SIDState.h"


AudioEffect* createEffectInstance(audioMasterCallback audioMaster)
{
   return new ReSID_VST(audioMaster);
} // createEffectInstance


const float ReSID_VST::DefaultSamplingRate = 44100.0f;


const unsigned int ReSID_VST::UpperOctave[12] = {
   35115, 37203, 39415, 41759, 44242, 46873, 49660, 52613, 55741, 59056, 62567, 66288,
};


unsigned short ReSID_VST::NoteTable[ReSID_VST::NoteTableSize];


ReSID_VST::ReSID_VST(audioMasterCallback audioMaster)
   :  AudioEffectX(audioMaster, 128, (int) Param::Count),
      _pNoteScheduler(NULL),
      _pVoiceAlloc(NULL),
      _pSIDState(NULL)
{
   if (audioMaster) {
      setNumInputs(0);
      setNumOutputs(2);
      canProcessReplacing();
      isSynth();
      setUniqueID('SID');
   }

   init();
} // ReSID_VST::ReSID_VST


void ReSID_VST::playMIDINote(unsigned char note, bool on /* = true */) {
   playTone(NoteTable[note - 12], on);
}

void ReSID_VST::setGate(bool state) {
   unsigned char reg = _pSIDState->Registers[0x04];
   
   if (state) reg |= 1;
   else reg &= ~1;

   _pSIDState->Registers[0x04] = reg;
   _pSIDState->Apply();
}

void ReSID_VST::playTone(unsigned short freq, bool gate)
{
   unsigned char waveFormAndGateReg = _pSIDState->Registers[0x04];
   if (gate) waveFormAndGateReg |= 1;
   else waveFormAndGateReg &= ~1;

   _pSIDState->Registers[0x00] = freq & 0xff;         // Frequency Low
   _pSIDState->Registers[0x01] = freq >> 8;           // Frequency high
   _pSIDState->Registers[0x04] = waveFormAndGateReg;
   _pSIDState->Apply();
} // ReSID_VST::playTone


void ReSID_VST::init() 
{
   static const unsigned short InitFreq = 1845;
   
   _pSid = new SID();
   _pSid->set_chip_model(ChipModel);
   _pSid->reset();
   _pSid->set_sampling_parameters(SidClockFreq, SamplingMethod, DefaultSamplingRate);

   initNoteTable();

   _pSIDState = new SIDState(_pSid);

   _pSIDState->Registers[0x05] = 0x00;             // Attack/Decay
   _pSIDState->Registers[0x06] = 0xfa;			      // Sustain/Release
   _pSIDState->Registers[0x00] = InitFreq & 0xff;	// Frequency Low
   _pSIDState->Registers[0x01] = InitFreq >> 8;    // Frequency High
   _pSIDState->Registers[0x04] = 0x20;             // saw + no gate
   _pSIDState->Registers[0x17] = 0x07;			      // Filter ON for all voices
   _pSIDState->Registers[0x15] = 500 & 0x07;		   // Cutoff lo
   _pSIDState->Registers[0x16] = 500 >> 3;		   // Cutoff hi
   _pSIDState->Registers[0x18] = 0x1f;			      // Volume, LP

   _pSIDState->Apply();

   _pVoiceAlloc = new MonophonicVoiceAlloc(this);
   _pNoteScheduler = new NoteScheduler(_pVoiceAlloc);
}


void ReSID_VST::initNoteTable()
{
   int quotient = 1 << Octaves;

   for (int i = 0; i < Octaves; ++i) {
      for (int j = 0; j < NotesPerOctave; ++j) {
         NoteTable[(i * NotesPerOctave) + j] =  (unsigned short) (((double) UpperOctave[j] / quotient) + .5);
      }

      quotient /= 2;
   }
}


ReSID_VST::~ReSID_VST()
{
   delete _pSid;
   delete _pNoteScheduler;
   delete _pVoiceAlloc;
   delete _pSIDState;
} // ReSID_VST::~ReSID_VST


int ReSID_VST::getNumMidiInputChannels()
{
   return 1;   // For now...
} // ReSID_VST::getNumMidiInputChannels


int ReSID_VST::processEvents(VstEvents* events)
{
	for (VstInt32 i = 0; i < events->numEvents; ++i)
	{
      VstEvent *pEvent = events->events[i];

      switch (pEvent->type) {
         case kVstMidiType:
            processMIDIEvent((VstMidiEvent*) pEvent);
            break;
      }
   }

   return 1;
} // ReSID_VST::processEvents


void ReSID_VST::processMIDIEvent(VstMidiEvent *pEvent)
{
   unsigned char *midiData = (unsigned char*) pEvent->midiData;

   int channel = midiData[0] & 0x0f;
   MidiStatus status = (MidiStatus) (midiData[0] & 0xf0);  // Get status id

   switch (status) {
      case MidiStatus::NoteOff:
      case MidiStatus::NoteOn:
         {
			   VstInt32 note = midiData[1] & 0x7f;
			   VstInt32 velocity = midiData[2] & 0x7f;

            if ((status == MidiStatus::NoteOff) || !velocity) {
               _pNoteScheduler->enqueue(MIDINote::NoteType::Off, note, velocity, pEvent->deltaFrames);
            }
            else {
               _pNoteScheduler->enqueue(MIDINote::NoteType::On, note, velocity, pEvent->deltaFrames);
            }
         }
         break;
   }
   // ToDo...
}


void ReSID_VST::setSampleRate(float sampleRate) 
{
   AudioEffectX::setSampleRate(sampleRate);
   _pSid->set_sampling_parameters(SidClockFreq, SamplingMethod, sampleRate);
} // ReSID_VST::setSampleRate


VstInt32 ReSID_VST::canDo(char *text)
{
	if (!strcmp(text, "receiveVstEvents")) return 1;
	if (!strcmp(text, "receiveVstMidiEvent")) return 1;
	if (!strcmp(text, "midiProgramNames")) return 1;
	return -1;	// explicitly can't do; 0 => don't know
} // ReSID_VST::canDo


void ReSID_VST::processReplacing(float** inputs, float** outputs, VstInt32 sampleFrames)
{
   _pNoteScheduler->nextFragment(sampleFrames);

   for (int i = 0; i < sampleFrames; ++i) {
      _pNoteScheduler->setPosition(i);

      short sampleBuf;
      cycle_count delta_t = 1;
      while (!_pSid->clock(delta_t, &sampleBuf, 1)) {
         if (!delta_t) delta_t = 1;
      }

      outputs[0][i] = (float)sampleBuf / 32768;
      outputs[1][i] = (float)sampleBuf / 32768;
   }

   _pNoteScheduler->fragmentDone();
} // ReSID_VST::processReplacing