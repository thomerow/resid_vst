#pragma once

#include <vector>

using std::vector;

struct MIDINote {   
   enum class NoteType {
      On, Off,
   };

   NoteType Type;
   unsigned char Note;
   unsigned char Velocity;
   int DeltaFrames;

   MIDINote(NoteType type, unsigned char note, unsigned char velocity, int deltaFrames)
      : Type(type), Note(note), Velocity(velocity), DeltaFrames(deltaFrames)
   {}
}; // struct MIDINote

typedef vector<MIDINote> NoteList;
