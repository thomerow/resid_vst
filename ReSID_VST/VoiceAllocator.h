#pragma once


class ReSID_VST;

/**
 * Abstarct voice allocator class.
 */
class VoiceAllocator 
{
public:

   VoiceAllocator(ReSID_VST *pSid) : _pSid(pSid) {}
   virtual ~VoiceAllocator() {};

   virtual void noteOn(unsigned char note) = 0;
   virtual void noteOff(unsigned char note) = 0;


protected:

   ReSID_VST *_pSid;
};