#include "SIDState.h"
#include "../resid-0.16/sid.h"
#include <algorithm>


/**
 * Registers are set in this order.
 */
const unsigned char SIDState::SIDOrder[RegisterCount] = {
   0x15, 0x16, 0x18, 0x17,
   0x05, 0x06, 0x02, 0x03, 0x00, 0x01, 0x04,
   0x0c, 0x0d, 0x09, 0x0a, 0x07, 0x08, 0x0b,
   0x13, 0x14, 0x10, 0x11, 0x0e, 0x0f, 0x12
};


SIDState::SIDState(SID *pSid)
   : _pSid(pSid)
{
   // Zero init registers
   std::fill(_registers, &_registers[sizeof(_registers)], 0);
} // SIDState::SIDState


void SIDState::Apply() {
   for (int i = 0; i < RegisterCount; ++i) {
      unsigned char reg = SIDOrder[i];
      _pSid->write(reg, _registers[reg]);
   }
} // SIDState::Apply