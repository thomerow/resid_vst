#pragma once

#include "VoiceAllocator.h"
#include "MIDINote.h"


class MonophonicVoiceAlloc : public VoiceAllocator 
{
public:

   MonophonicVoiceAlloc(ReSID_VST *pSid) : VoiceAllocator(pSid) {}

   virtual void noteOn(unsigned char note);
   virtual void noteOff(unsigned char note);

private:

   vector<unsigned char> _notes;
}; // class MonophonicVoiceAlloc