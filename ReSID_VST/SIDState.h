#pragma once


class SID;

class SIDState
{
public:

   static const int RegisterCount = 0x19;

   SIDState(SID *pSid);

   void Apply();

   __declspec(property(get = getRegisters)) unsigned char* Registers;

   unsigned char* getRegisters() { return _registers; }

private:

   static const unsigned char SIDOrder[RegisterCount];

   SID            *_pSid;
   unsigned char  _registers[RegisterCount];
}; // class SIDState