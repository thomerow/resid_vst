#pragma once


#include "../public.sdk/source/vst2.x/audioeffectx.h"
#include "resid-0.16/siddefs.h"
#include <vector>


using std::vector;


class SID;
class VoiceAllocator;
class NoteScheduler;
class SIDState;

class ReSID_VST : public AudioEffectX
{
public:

   enum class Param {
      WaveForm_Voice_0,
      WaveForm_Voice_1,
      WaveForm_Voice_2,

      // ToDo: complete parameters

      Count,
   }; // enum class Param

   enum class MidiStatus {
      NoteOff = 0x80,
      NoteOn = 0x90,
      
   };

   // static const int Sids = 2;
   static const sampling_method SamplingMethod = SAMPLE_RESAMPLE_INTERPOLATE; // SAMPLE_INTERPOLATE;
   static const float DefaultSamplingRate;
   static const int SidClockFreq = 985250;      // PAL
   // static const int SidClockFreq = 1022730;      // NTSC
   static const int EngineClockFreq = 1000;		// Hz
   static const chip_model ChipModel = MOS8580;

   ReSID_VST(audioMasterCallback audioMaster);
   ~ReSID_VST();

   virtual void setSampleRate(float sampleRate);
   virtual void processReplacing(float** inputs, float** outputs, VstInt32 sampleFrames);
   virtual int getNumMidiInputChannels();
	virtual VstInt32 processEvents (VstEvents* events);
   virtual VstInt32 canDo(char* text);

   void playMIDINote(unsigned char note, bool on = true);
   void setGate(bool state);

private:

   // Statics and consts:

   static const int           NotesPerOctave = 12;
   static const int           Octaves = 8;
   static const int           NoteTableSize = Octaves * NotesPerOctave;
   static const unsigned int  UpperOctave[NotesPerOctave];
   static unsigned short      NoteTable[NoteTableSize];
   
   // Fields:

   SID               *_pSid;
   VoiceAllocator    *_pVoiceAlloc;
   NoteScheduler     *_pNoteScheduler;
   SIDState          *_pSIDState;

   // Methods:

   void initNoteTable();
   void playTone(unsigned short freq, bool gate);
   void processMIDIEvent(VstMidiEvent *pEvent);
   void init();
}; // class ReSID_VST