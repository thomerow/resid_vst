#pragma once

#include "MIDINote.h"
#include <algorithm>

class VoiceAllocator;

class NoteScheduler
{
public:

   NoteScheduler(VoiceAllocator *pVoiceAlloc);

   void enqueue(MIDINote::NoteType type, unsigned char note, unsigned char velocity, int deltaFrames);
   void setPosition(int nFrame);
   void nextFragment(int nFrames);
   void fragmentDone();


private:

   int                  _nFrames;       // Length of current sample fragment
   NoteList             _notes;
   NoteList             _futureNotes;
   VoiceAllocator       *_pVoiceAlloc;
   int                  _nNextNote;

   static void sortNotes(NoteList &notes);
}; // class NoteScheduler


inline void NoteScheduler::sortNotes(NoteList &notes)
{
   std::sort(notes.begin(), notes.end(), [](const MIDINote &a, const MIDINote &b) { return a.DeltaFrames < b.DeltaFrames; });
} // NoteScheduler::sortNotes
