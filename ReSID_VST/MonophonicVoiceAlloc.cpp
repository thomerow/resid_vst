#include "MonophonicVoiceAlloc.h"
#include "ReSID_VST.h"
#include <algorithm>


void MonophonicVoiceAlloc::noteOn(unsigned char note)
{
   if (_notes.empty() || (std::find(_notes.begin(), _notes.end(), note) == _notes.end())) {
      _pSid->playMIDINote(note);
      _notes.push_back(note);
   }
}


void MonophonicVoiceAlloc::noteOff(unsigned char note)
{
   if (_notes.empty()) return;

   if (_notes.back() == note) {
      // Remove note from note list
      _notes.pop_back();

      // Play previously pressed note or toggle gate
      if (!_notes.empty()) {
         _pSid->playMIDINote(_notes.back());
      } 
      else _pSid->setGate(false);
   }
   else {
      vector<unsigned char>::iterator iNote = std::find(_notes.begin(), _notes.end(), note);
      if (iNote != _notes.end()) {
         _notes.erase(iNote);
      }
   }
}